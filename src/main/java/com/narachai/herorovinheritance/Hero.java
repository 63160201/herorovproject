/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.herorovinheritance;

/**
 *
 * @author ASUS
 */
public class Hero {
    protected String name;
    protected int maxHp =0;
    protected int attackDamage =0;
    protected int abilityPower =0;
    protected int armor =0;
    protected int magicDefense =0;
    protected int maxMana =0;
    public Hero(String name,int maxHp,int attackDamage,int abilityPower,int armor,int magicDefense,int maxMana){
        System.out.println("Hero created");
        this.name = name;
        this.maxHp = maxHp;
        this.attackDamage = attackDamage;
        this.abilityPower = abilityPower;
        this.armor = armor;
        this.magicDefense = magicDefense;
        this.maxMana = maxMana;
    }
    public void herospeak() {
        System.out.println("I am Hero!!!");
    }

    public String getName() {
        return name;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public int getAttackDamage() {
        return attackDamage;
    }

    public int getAbilityPower() {
        return abilityPower;
    }

    public int getArmor() {
        return armor;
    }

    public int getMagicDefense() {
        return magicDefense;
    }

    public int getMaxMana() {
        return maxMana;
    }
    
    
}
