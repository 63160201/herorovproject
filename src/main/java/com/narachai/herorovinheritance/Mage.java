/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.herorovinheritance;

/**
 *
 * @author ASUS
 */
public class Mage extends Hero{
    public Mage(String name,int maxHp,int attackDamage,int abilityPower,int armor,int magicDefense,int maxMana){
        super(name,maxHp,attackDamage,abilityPower,armor,magicDefense,maxMana);
        System.out.println("Mage created");
    }
    @Override
    public void herospeak(){
        System.out.println("I am Mage!!!");
    }
    
    @Override
    public String toString(){
        return "Name = "+name+"\nMaxHp = "+maxHp+"\nAttackDamage = "
                +attackDamage+"\nAbilityPower = "+abilityPower+"\nArmor = "
                +armor+"\nMagicDefense = "+magicDefense+"\nMaxMana = "+maxMana
                +"\n*******************************";
    }
    
}
