/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.herorovinheritance;

/**
 *
 * @author ASUS
 */
public class TestHero {
    public static void main(String[] args) {
        Hero hero = new Hero("Hero",0,0,0,0,0,0);
        hero.herospeak();
        Assassin butterfly = new Assassin("Butterfly",3619,177,0,135,80,0);
        butterfly.herospeak();
        System.out.println(butterfly);
        Assassin wukong = new Assassin("WuKong",3505,167,0,139,80,430);
        wukong.herospeak();
        System.out.println(wukong);
        Carey valhein = new Carey("Valhein",3592,173,0,141,80,440);
        valhein.herospeak();
        System.out.println(valhein);
        Mage zata = new Mage("Zata",3585,153,0,141,80,490);
        zata.herospeak();
        System.out.println(zata);
        Mage lauriel = new Mage("Lauriel",3369,167,0,133,80,490);
        lauriel.herospeak();
        System.out.println(lauriel);
        Support alice = new Support("Alice",3558,176,0,144,80,470);
        alice.herospeak();
        System.out.println(alice);
        Tank thane = new Tank("Thane",3835,157,0,166,80,420);
        thane.herospeak();
        System.out.println(thane);
        
    }
}
